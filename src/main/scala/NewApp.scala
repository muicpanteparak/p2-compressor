import java.io.{File, InputStream}

import MainApplication.{LOG, consumer, rabbit}
import com.google.gson.{JsonArray, JsonObject}
import me.teparak.minio.utils.MinioWrapper
import me.teparak.rabbit.utils.Rabbit
import org.apache.commons.io.{FileUtils, FilenameUtils, IOUtils}
import org.slf4j.LoggerFactory

import scala.sys.process._
import scala.util.Properties.envOrElse

object NewApp extends App {
  private val MINIO_HOST = envOrElse("MY_MINIO_HOST", "localhost")
  private val MINIO_PORT = envOrElse("MY_MINIO_PORT", "9000").toInt
  private val MINIO_USER = envOrElse("MY_MINIO_USER", "admin")
  private val MINIO_PASS = envOrElse("MY_MINIO_PASS", "password")
  private val RABBIT_HOST = envOrElse("MY_RABBIT_HOST", "localhost")
  private val RABBIT_PORT = envOrElse("MY_RABBIT_PORT", "5672").toInt
  private val RABBIT_USER = envOrElse("MY_RABBIT_USER", "guest")
  private val RABBIT_PASS = envOrElse("MY_RABBIT_PASS", "guest")
  println(MINIO_HOST, MINIO_PORT, MINIO_USER, MINIO_PASS)
  println(RABBIT_HOST, RABBIT_PORT, RABBIT_USER, RABBIT_PASS)
  private val minio = new MinioWrapper(MINIO_HOST, MINIO_PORT, MINIO_USER, MINIO_PASS)
  private val rabbit = new Rabbit(RABBIT_HOST, RABBIT_PORT, RABBIT_USER, RABBIT_PASS)
  private val LOG = LoggerFactory.getLogger(this.getClass.getName)
  private val TXT_LOCATION = "txt/"


  def downloadFromMinio(job_id: String, objectId: String): (InputStream, Long) = {
    LOG.info(s"Downloading '$job_id'/'$objectId' from minio")
    val stat = minio.getMetadata(job_id, objectId)
    val soc = minio.download(job_id, objectId)
    (soc, stat.getContentLength)
  }

  def uploadToMinio(job_id: String, object_id: String, file: File): Unit ={
    LOG.info(s"Uploading $job_id/$object_id to minio")
    minio.upload(job_id, s"$object_id-final", file)
    LOG.debug("Uploaded complete $job_id/$object_id")
  }

  def removeAll(files: List[File]): Unit = {
    files.foreach(removeFile)
  }

  def removeFile(file: File): Unit ={
    file.delete()
  }

  def initiate(jobId: String, files: JsonArray) = {
    val objects = (0 until files.size())
      .toList
      .map(i => FilenameUtils.getBaseName(files.get(i).getAsString))
      .distinct

    val lstDownloadFiles = objects
      .map(ob => {
        val (in, _) = downloadFromMinio(jobId, TXT_LOCATION + ob + ".txt")
        val file = File.createTempFile("text-process", ".txt")
        FileUtils.copyToFile(in, file)
        file
    })

    val out = new File("out.tar.gz")

//    def e(): Seq[String] = Seq("tar", "-czf", out.getAbsolutePath) ++ downloadFiles.map(_.getAbsolutePath)

    val e: Seq[String] = Seq("tar", "-czf",  out.getAbsolutePath) ++ lstDownloadFiles.map(_.getAbsolutePath)
    val code: Int = e.!

    if (code != 0){
      throw new RuntimeException("failed to process job")
    }
    uploadToMinio(jobId,jobId, out)
    removeAll(lstDownloadFiles)
    removeFile(out)
    notifyMaster(jobId)
    LOG.info(f"$jobId compressed")
  }

  def notifyMaster(jobId: String): Unit ={
    val jsonComplete = new JsonObject
    jsonComplete.addProperty("jobId", jobId)
    jsonComplete.addProperty("jobType", "compression_complete")
    jsonComplete.addProperty("status", true)

    rabbit.publish(jsonComplete, "status")
  }

  def consumer(json: JsonObject) = (json.get("job_id").getAsString, json.get("files").getAsJsonArray) match {
    case (job_id: String, files: JsonArray) => initiate(job_id, files)
    case _ => throw new IllegalArgumentException("job_id does not exist")
  }

  def main() = {
    LOG.info("Initializing RabbitMQ")
    rabbit.listen("compress", consumer _, true)
  }
  main()
}