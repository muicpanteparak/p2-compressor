import java.io._

import Pipe.CompressionUtils
import com.google.gson.{JsonArray, JsonObject}
import me.teparak.minio.utils.MinioWrapper
import me.teparak.rabbit.utils.Rabbit
import org.apache.commons.io.FilenameUtils
import org.slf4j.LoggerFactory

import scala.util.Properties.envOrElse


object MainApplication {
  private val MINIO_HOST = envOrElse("MY_MINIO_HOST", "localhost")
  private val MINIO_PORT = envOrElse("MY_MINIO_PORT", "9000").toInt
  private val MINIO_USER = envOrElse("MY_MINIO_USER", "admin")
  private val MINIO_PASS = envOrElse("MY_MINIO_PASS", "password")

  private val RABBIT_HOST = envOrElse("MY_RABBIT_HOST", "localhost")
  private val RABBIT_PORT = envOrElse("MY_RABBIT_PORT", "5672").toInt
  private val RABBIT_USER = envOrElse("MY_RABBIT_USER", "guest")
  private val RABBIT_PASS = envOrElse("MY_RABBIT_PASS", "guest")

  println(MINIO_HOST, MINIO_PORT, MINIO_USER, MINIO_PASS)
  println(RABBIT_HOST, RABBIT_PORT, RABBIT_USER, RABBIT_PASS)
  private val minio = new MinioWrapper(MINIO_HOST, MINIO_PORT, MINIO_USER, MINIO_PASS)
  private val rabbit = new Rabbit(RABBIT_HOST, RABBIT_PORT, RABBIT_USER, RABBIT_PASS)
  private val compressionUtils = new CompressionUtils(downloadFile, uploadFile)
  private val LOG = LoggerFactory.getLogger(this.getClass.getName)
  private val TXT_LOCATION = "txt/"
//  private val minio = new MinioWrapper(MINIO_HOST, MINIO_PORT)
  LOG.debug(s"[x] MINIO Connected at $MINIO_HOST:$MINIO_PORT")
//  private val rabbit = new Rabbit(RABBIT_HOST, RABBIT_PORT)
  LOG.debug(s"[x] RABBITMQ Connected at $RABBIT_HOST:$RABBIT_PORT")
//  private val compressionUtils = new CompressionUtils(downloadFile, uploadFile)
  //  InputStream(file) -> OutputStream (gip) -> InputStream (Upload back to Server)

  def downloadFile(job_id: String, objectId: String): (InputStream, Long) = {
    LOG.info(s"Downloading '$job_id'/'$objectId' from minio")
    val stat = minio.getMetadata(job_id, objectId)
    val soc = minio.download(job_id, objectId)
    (soc, stat.getContentLength)
  }

  def uploadFile(job_id: String, object_id: String, in: InputStream): Unit ={
    LOG.info(s"Uploading $job_id/$object_id to minio")
    minio.upload(job_id, s"$object_id-final", in)
    LOG.debug("Uploaded complete $job_id/$object_id")
  }

//  def testt(): Unit = {
//    val compressor = new CompressionUtils(download_helper, uploadFile)
//    val lst = new ArrayBuffer[(String, String)]()
//
//    lst.append(
//      ("test", "Avengers_ Infinity War (2018) Thor arrives in Wakanda Scene.mp4"),
//      ("test", "ComSecExploit1.mov"),
//    )
//
//    compressor.pushToStream(lst)
//
//    var th = new Thread(() => {
//      uploadFile("test", "out" + ".tar.gz", compressor.getOutStream())
//    })
//
//    th.start()
//    compressor.closeStreams()
//    th.join()
//  }


  def consumer(json: JsonObject) = {
    // TODO: catch null pointer exception

    println("New Job")
    (json.get("job_id").getAsString, json.get("files").getAsJsonArray) match {
      case (job_id: String, files: JsonArray) =>
//        println(files.get())
//        val objects =  List(files).map {
//          a => println(a)
//          FilenameUtils.getBaseName(a.getAsString)
//        }
        val objects = (0 until files.size())
          .toList
          .map(i => FilenameUtils.getBaseName(files.get(i).getAsString))
            .distinct

        println(f"Received new job $job_id/$objects ")


        compressionUtils.createStream()
        compressionUtils.pullFromStream(job_id, s"$job_id.tar.gz")
        compressionUtils.pushToStream(objects.map(ob => (job_id, TXT_LOCATION + ob + ".txt")))
        compressionUtils.join()
        compressionUtils.closeStreams()
        val jsonComplete = new JsonObject
        jsonComplete.addProperty("jobId", job_id)
        jsonComplete.addProperty("jobType", "compression_complete")
        jsonComplete.addProperty("status", true)
        rabbit.publish(jsonComplete, "status")

        println("Done")
      case _ => throw new IllegalArgumentException("job_id does not exist")
    }
  }


  def main() = {
    LOG.info("Initializing RabbitMQ")
    rabbit.listen("compress", consumer _, true)
  }

  main()
}