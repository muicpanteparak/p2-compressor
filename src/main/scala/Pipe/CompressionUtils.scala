package Pipe

import java.io._
import java.util.concurrent._
import java.util.zip.GZIPOutputStream

import org.apache.commons.compress.archivers.tar.{TarArchiveEntry, TarArchiveOutputStream}
import org.apache.commons.io.IOUtils
import org.slf4j.LoggerFactory

import scala.collection.mutable.ArrayBuffer

class CompressionUtils(incoming: (String, String) => (InputStream, Long), outgoing: (String, String, InputStream) => Unit) {
  private var isOpen = false
  private var stop = false
  private var resultStream: PipedInputStream = null
  private var tarArchiveOutputStream: TarArchiveOutputStream = null
  private val pushStreamPool = Executors.newFixedThreadPool(1)
  private val pullStreamPool = Executors.newFixedThreadPool(1)
  private val futures: ArrayBuffer[Future[(String, String)]] = new ArrayBuffer[Future[(String, String)]]()
  private val LOG = LoggerFactory.getLogger(this.getClass.getName)


  private def getCompressionWrapper(sourceStream: OutputStream): TarArchiveOutputStream = {
    new TarArchiveOutputStream(new GZIPOutputStream(new BufferedOutputStream(sourceStream, 1024 * 16), 1024 * 8), 1024 * 4)
  }

  def createStream(): Unit = {
    if (isOpen){
      println("Stream is already open")
    }
    isOpen = true

    resultStream = new PipedInputStream
    tarArchiveOutputStream = getCompressionWrapper(new PipedOutputStream(resultStream))

  }

  def pushToStream(work: List[(String, String)]): Unit = {
    println("pushing to stream")

    val a = work.map {
      case (job_id: String, object_id: String) => pushStreamPool.submit(new Work(job_id, object_id))
    }
    futures.insertAll(futures.length, a)

    val n = futures.size

    println(f"pushed to stream $n")
  }

  def pullFromStream(jobId: String, objectId: String): Unit = {
    pullStreamPool.submit(new Runnable {
      override def run(): Unit = {
        outgoing(jobId, objectId, getOutStream())
      }
    })
  }

  private def getOutStream(): InputStream = {
    resultStream
  }

  def closeStreams(): Unit = {
    if (!isOpen){
      println("Stream not open")
      return
    }

    IOUtils.closeQuietly(resultStream)
    IOUtils.closeQuietly(tarArchiveOutputStream)
    isOpen = false
    println("Stream Closed")
  }

  def shutdown(): Unit ={
    isOpen = false
    stop = true
    pullStreamPool.shutdown()
    pushStreamPool.shutdown()
    LOG.debug("Shutdown completed")
  }

//  private def canSubmitJob(): Unit ={
//    !isOpen && !stop
//  }

  def join(f: Any => Any): Unit = {
    LOG.debug("waiting for jobs to finish")
    futures.foreach(elt => {
      println(elt.get())
    })
    LOG.debug("all jobs completed closing")
  }

  def join(): Unit = {
    join(null)
    println("Joined Stream ")
  }

  class Work(jobId: String, objectId: String) extends Callable[(String, String)] {

    override def call(): (String, String) = {

      try {
        println(s"Compressing $jobId/$objectId")
        val (in: InputStream, size: Long) = incoming(jobId, objectId)
        val entry = new TarArchiveEntry(objectId)
        entry.setSize(size)

        tarArchiveOutputStream.putArchiveEntry(entry)

        IOUtils.copy(in, tarArchiveOutputStream)
        IOUtils.closeQuietly(in)
      } catch {
        case e: Exception => e.printStackTrace()
      } finally {
        tarArchiveOutputStream.closeArchiveEntry()
      }

      (jobId, objectId)
    }
  }
}

sealed trait Empty