#FROM hseeberger/scala-sbt
#WORKDIR /app
#ADD src src
#ADD build.sbt build.sbt
#CMD sbt run

# FROM panteparak/wait-for as waitfor
# COPY --from=waitfor /wait-for /app/wait-for

FROM openjdk:8-alpine
RUN apk add --update --no-cache bash tar

WORKDIR /app
# RUN chmod +x wait-for
COPY target/scala-2.12/compressor-assembly-0.1.jar compressor.jar
#CMD ./wait-for redis:6379 -s -q -t 30 -- ./wait-for minio:9000 -s -q -t 30 --

#ADD https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh wait-for-it
#RUN chmod +x wait-for-it
#CMD ./wait-for-it minio:9000 -s -t 30 -- ./wait-for-it rabbitmq:5672 -s -t 30 -- java -Xms512m -Xmx2g -jar compressor.jar
CMD java -Xms512m -Xmx2g -jar compressor.jar