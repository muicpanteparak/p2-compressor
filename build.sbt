name := "compressor"

version := "0.1"

scalaVersion := "2.12.7"

resolvers += "Artifactory" at "https://artifact.teparak.me/artifactory/backendtech/"


libraryDependencies += "commons-io" % "commons-io" % "2.6"

libraryDependencies += "org.apache.commons" % "commons-compress" % "1.18"

libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.5" % "provided"

libraryDependencies += "com.google.code.gson" % "gson" % "2.8.5"

libraryDependencies += "me.teparak" % "rabbitmq" % "1.1.2"

libraryDependencies += "me.teparak" % "minio" % "1.2.2"

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}
